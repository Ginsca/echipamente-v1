package com.echipamente.echipamente.entity;


import javax.persistence.*;
import java.util.List;


@Entity public class Contacte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String cod;
    private String nume;
    private String observatii;

    @ManyToMany(mappedBy = "contacte")
    private List<Echipamente> echipamente;

    public static void deleteById(int id) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getObservatii() {
        return observatii;
    }

    public void setObservatii(String observatii) {
        this.observatii = observatii;
    }

    public List<Echipamente> getEchipamente() {
        return echipamente;
    }

    public void setEchipamente(List<Echipamente> echipamente) {
        this.echipamente = echipamente;
    }
}

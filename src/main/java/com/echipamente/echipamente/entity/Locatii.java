package com.echipamente.echipamente.entity;
import javax.persistence.*;
import java.util.List;


@Entity
public class Locatii {

    public Locatii(){}

    public Locatii(int id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    @Column
    private String nume;

//    @OneToMany(mappedBy = "locatie")
//    private List<Echipamente> echipamente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

//    public List<Echipamente> getEchipamente() {
//        return echipamente;
//    }

//    public void setEchipamente(List<Echipamente> echipamente) {
//        this.echipamente = echipamente;
//    }
}
package com.echipamente.echipamente.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity

public class Echipamente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int cod;
    private String nume;
    private String producator;
    private String model;
    private String observatie;

    @ManyToOne
    @JoinColumn(name = "actionare")
    private Actionare actionare;

    @ManyToOne
    @JoinColumn(name = "locatie")
    private Locatii locatie;

    @ManyToOne
    @JoinColumn(name = "tip_echipament")
    private Tipuri tip;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "echipamente_has_contacte",
            joinColumns = @JoinColumn(name = "echipamente_id"),
            inverseJoinColumns = @JoinColumn(name = "contacte_id"))
    private List<Contacte> contacte;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getObservatie() {
        return observatie;
    }

    public void setObservatie(String observatie) {
        this.observatie = observatie;
    }

    public Actionare getActionare() {
        return actionare;
    }

    public void setActionare(Actionare actionare) {
        this.actionare = actionare;
    }

    public Locatii getLocatie() {
        return locatie;
    }

    public void setLocatie(Locatii locatie) {
        this.locatie = locatie;
    }

    public Tipuri getTip() {
        return tip;
    }

    public void setTip(Tipuri tip) {
        this.tip = tip;
    }

    public List<Contacte> getContacte() {
        return contacte;
    }

    public void setContacte(List<Contacte> contacte) {
        this.contacte = contacte;
    }


//    public  List<Locatii> getLocationList() {
//        if (locationList == null){
//            locationList = new ArrayList<Locatii>();
//            locationList.add(new Locatii(1, "Cluj"));
//            locationList.add(new Locatii(2, "Luna"));
//            locationList.add(new Locatii(3, "Dej"));
//            locationList.add(new Locatii(4, "Olanda"));
//        }
//        return locationList;
//    }
//
//    public  void setLocationList(List<Locatii> locationList) {
//        Echipamente.locationList = locationList;
//    }
//
//    private static List<Locatii> locationList = null;


}
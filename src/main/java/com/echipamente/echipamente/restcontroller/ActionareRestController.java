package com.echipamente.echipamente.restcontroller;

import com.echipamente.echipamente.entity.Actionare;
import com.echipamente.echipamente.service.ActionareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RestController
//@RequestMapping("/actionare")
public class ActionareRestController {

    @Autowired
    private ActionareService service;

    @GetMapping("/actionare")
    public List<Actionare> getAllActionare() {
        return service.getAll();
    }
    @PostMapping("/actionare")
    public ResponseEntity<Actionare> createActionare(@RequestBody Actionare actionare) {

        Actionare result = service.create(actionare);
        return new ResponseEntity<>(result, HttpStatus.CREATED);

    }
}

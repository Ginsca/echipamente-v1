package com.echipamente.echipamente.restcontroller;

import com.echipamente.echipamente.entity.Locatii;
import com.echipamente.echipamente.service.LocatiiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RestController
//@RequestMapping ("/api/v1")
public class LocatiiRestController {

    @Autowired
    @Qualifier("locationService") // not required but good practice
    private LocatiiService service;

    @GetMapping("/locatii")
    public List<Locatii> getAllLocatii() {
        return service.getAll();
    }

    @PostMapping("/locatii")
    public ResponseEntity<Locatii> createLocatii(@RequestBody Locatii locatii) {
        Locatii result = service.create(locatii);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}



















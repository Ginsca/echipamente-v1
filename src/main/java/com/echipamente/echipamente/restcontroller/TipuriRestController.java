package com.echipamente.echipamente.restcontroller;

import com.echipamente.echipamente.entity.Tipuri;
import com.echipamente.echipamente.service.TipuriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RestController
//@RequestMapping("/api/v1")
public class TipuriRestController {


    @Autowired
    @Qualifier("tipuriService")
    private TipuriService service;

    @GetMapping("/tipuri")
    public List<Tipuri> getAllTipuri() {
        return service.getAll();
    }

    @PostMapping("/tipuri")
    public ResponseEntity<Tipuri> createTipuri(@RequestBody Tipuri tipuri) {
        Tipuri result = service.create(tipuri);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }















}

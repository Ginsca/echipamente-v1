package com.echipamente.echipamente.restcontroller;

import com.echipamente.echipamente.entity.Echipamente;
import com.echipamente.echipamente.service.EchipamenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RestController
//@RequestMapping("/api/v1")
public class EchipamenteRestController {

    @Autowired
    private EchipamenteService service;

    @GetMapping("/echipamente")
    public List<Echipamente> getAllEquipments() {
        return service.getAll();
    }

    @PostMapping("/echipamente")
    public ResponseEntity<Echipamente> createEchipamente(@RequestBody Echipamente echipamente) {
        Echipamente result = service.create(echipamente);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
package com.echipamente.echipamente.restcontroller;


import com.echipamente.echipamente.entity.Contacte;
import com.echipamente.echipamente.service.ContacteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RestController
//@RequestMapping ("/api/v1")
public class ContacteRestController {
    @Autowired
    private ContacteService service;

    @GetMapping("/contacte")
    public List<Contacte>getAllContacte(){return service.getAll();}

    @PostMapping("/contacte")
    public ResponseEntity<Contacte> createContacte(@RequestBody Contacte contacte) {

        Contacte result = service.create(contacte);
        return new ResponseEntity<>(result, HttpStatus.CREATED);

    }
}

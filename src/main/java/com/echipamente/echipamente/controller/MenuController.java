package com.echipamente.echipamente.controller;


import com.echipamente.echipamente.entity.Contacte;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MenuController {

    @GetMapping("/")
    public String showMenu(Model model) {
        return "Menu";
    }

}

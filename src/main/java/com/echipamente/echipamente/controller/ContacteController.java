package com.echipamente.echipamente.controller;

import com.echipamente.echipamente.entity.Contacte;
import com.echipamente.echipamente.entity.Echipamente;
import com.echipamente.echipamente.entity.Locatii;
import com.echipamente.echipamente.service.ContacteService;
import com.echipamente.echipamente.service.LocatiiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/contacte")
public class ContacteController {

    @Autowired
    private ContacteService contacteService;
    @Autowired
    private LocatiiService locatiiService;

    @GetMapping("/contacte")
    public String getContactCreationForm(Model model) {
        model.addAttribute("contacte", new Contacte());
        return "createContact";
    }

    @GetMapping("/create")
    public String getEquipmentCreationForm(Model model) {
        model.addAttribute("contacte", new Contacte());
        return "createContact";
    }

    @PostMapping("/create")
    public String registerContact(@ModelAttribute(name = "contacte") Contacte contacte, Model model) {
        Contacte result = contacteService.create(contacte);
        model.addAttribute("contact", result);
        return "contactCreatedPage";
    }

    @GetMapping("/startSearch")
    public String showSearchForm(Model model) {
        model.addAttribute("contacte", new Contacte());
        return "startSearchContactsForm";
    }


    @PostMapping("/findByParams")
    public String showSearchResults(@ModelAttribute(name = "contact") Contacte contact, Model model) {
        Contacte result = contacteService.getByCode(contact.getCod());
        model.addAttribute("contact", result);
        List<Locatii> allLocations = locatiiService.getAll();
            //hack: we use model field to transport the location name
        for(Echipamente echipament: result.getEchipamente()) {
            int locationId = echipament.getLocatie().getId();
            Optional<Locatii> locationOptional = allLocations.stream().filter(l -> l.getId() == locationId).findFirst();
            String locationName = locationOptional.get().getNume();
            echipament.setModel(locationName);
        }
        return "showContactsSearchResult.html";
    }

    @GetMapping("/{cod}")
    public String getContactByCode(@PathVariable("code") String code, Model model) {
        Contacte result = contacteService.getByCode(code);
        model.addAttribute("contacte", result);
        return "getContactByCode";
    }

    @DeleteMapping("/contacte/{id}")
    void deleteContacte(@PathVariable int id) {
        Contacte.deleteById(id);
    }
    }


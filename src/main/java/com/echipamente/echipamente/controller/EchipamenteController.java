package com.echipamente.echipamente.controller;

import com.echipamente.echipamente.entity.Echipamente;
import com.echipamente.echipamente.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/echipamente")
public class EchipamenteController {

    @Autowired
    private EchipamenteService echipamenteService;
    @Autowired
    private LocatiiService locatiiService;
    @Autowired
    private ActionareService actionareService;
    @Autowired
    private ContacteService contacteService;


    @GetMapping("/create")
    public String getEquipmentCreationForm(Model model) {
        model.addAttribute("echipamente", new Echipamente());
        model.addAttribute("locations", locatiiService.getAll());
        model.addAttribute("action", actionareService.getAll());

        return "createEquipment";
    }

    @PutMapping("/update")
    public String updateEchipamente(@ModelAttribute(name = "echipamente") Echipamente echipamente, Model model) {
        Echipamente result = echipamenteService.update(echipamente);
        return "updateResult";
    }

    @GetMapping("/updateContacts")
    public String updateContacts(Model model) {
        model.addAttribute("echipamente", new Echipamente());
        return "linkEquipmentContact";
    }

    @PostMapping("/updateContacts")
    public String linkContacts(@ModelAttribute(name = "echipamente") Echipamente echipamente, Model model) {
        int equipementCode = echipamente.getCod();
        String contactCode = echipamente.getObservatie();
        echipamenteService.addContact(equipementCode, contactCode);
        return "linkEquipmentContact";
    }

    @GetMapping("/find")
    public String searchEquipment(Model model) {
        model.addAttribute("echipamente", new Echipamente());
        return "findEquipment";
    }


    @GetMapping("/{code}")
    public String getEquipmentByCode(@PathVariable("code") int code, Model model) {
        Echipamente result = echipamenteService.getByCode(code);
        if (result.getId() == 0) {
            model.addAttribute("errorMessage", "Nu s-a gasit echipamentul");
            return "error";
        } else {
            model.addAttribute("echipament", result);
            return "getEquipmentByCode";
        }
    }

    @PostMapping("/create")
    public String registerEquipment(@ModelAttribute(name = "echipamente") Echipamente echipamente, Model model) {
        echipamente.setTip(null);   // because table tip is not used anymore; we use column model instead
        Echipamente result = echipamenteService.create(echipamente);
        model.addAttribute("equipment", result);
        return "equipmentCreatedPage";
    }


    @PostMapping("/findByParams")
    public String getEquipmentByCode(@ModelAttribute(name = "echipamente") Echipamente echipamente, Model model) {
        int code = echipamente.getCod();
        Echipamente result = echipamenteService.getByCode(code);
        if (result.getId() == 0) {
            model.addAttribute("errorMessage", "Nu s-a gasit echipamentul");
            return "error";
        } else {
            model.addAttribute("echipament", result);
            return "getEquipmentByCode";
        }
    }

        @GetMapping("/delete/{id}")
    public String deleteEchipamente(@PathVariable("id") int id, Model model) {
        boolean successfullyDeleted = true;
        try {
            echipamenteService.delete(id);
        } catch (EmptyResultDataAccessException ex) {
            successfullyDeleted = false;
        }
        model.addAttribute("success", new Boolean(successfullyDeleted));
        return "equipmentDeletedPage";
    }
}

package com.echipamente.echipamente.repository;

import com.echipamente.echipamente.entity.Actionare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
/*JPA Java Persistence API (JPA) este specificația unei interfețe de programare a aplicațiilor (API) care
 descrie managementul datelor relaționale în aplicații ce folosesc platformele Java Standard Edition
 (SE) și Java Enterprise Edition (JEE).*/
public interface ActionareRepository extends JpaRepository <Actionare,Integer> {
}

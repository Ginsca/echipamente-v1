package com.echipamente.echipamente.repository;



import com.echipamente.echipamente.entity.Tipuri;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipuriRepository extends JpaRepository<Tipuri, Integer> {
}

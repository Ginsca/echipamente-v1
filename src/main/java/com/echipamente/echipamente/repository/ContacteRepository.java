package com.echipamente.echipamente.repository;

import com.echipamente.echipamente.entity.Contacte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContacteRepository extends JpaRepository <Contacte ,String> {
    Optional<Contacte> findFirstByCod(String cod);
}

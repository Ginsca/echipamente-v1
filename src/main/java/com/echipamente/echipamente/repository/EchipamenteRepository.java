package com.echipamente.echipamente.repository;

import com.echipamente.echipamente.entity.Echipamente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface EchipamenteRepository extends JpaRepository<Echipamente,Integer>{

    Optional<Echipamente> findFirstByCod(int cod);

}

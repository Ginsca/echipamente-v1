package com.echipamente.echipamente.repository;

import com.echipamente.echipamente.entity.Locatii;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocatiiRepository extends JpaRepository<Locatii, Integer> {
}

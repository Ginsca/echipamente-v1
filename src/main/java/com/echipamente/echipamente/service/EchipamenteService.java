package com.echipamente.echipamente.service;

import com.echipamente.echipamente.entity.Echipamente;

import java.util.List;

public interface EchipamenteService {
    Echipamente create (Echipamente echipamente);
    Echipamente getByCode(Integer code);
    List<Echipamente>getAll();
    Echipamente update(Echipamente echipamente);
    boolean delete(int id);
    void addContact(int equipmentCode, String contactCode);
}

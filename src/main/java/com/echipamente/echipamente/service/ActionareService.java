package com.echipamente.echipamente.service;

import com.echipamente.echipamente.entity.Actionare;

import java.util.List;

public interface ActionareService {
    Actionare create (Actionare actionare);
    List <Actionare>getAll();
    Actionare update(Actionare actionare);
    void delete (int id);

}

package com.echipamente.echipamente.service;

import com.echipamente.echipamente.entity.Contacte;

import java.util.List;

public interface ContacteService {
    Contacte create (Contacte contacte);
    List<Contacte> getAll();
    Contacte update(Contacte contacte);
    void delete(String id);
    Contacte getByCode(String code);
}


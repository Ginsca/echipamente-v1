package com.echipamente.echipamente.service.impl;

import com.echipamente.echipamente.entity.Contacte;
import com.echipamente.echipamente.repository.ContacteRepository;
import com.echipamente.echipamente.service.ContacteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContacteServiceImpl implements ContacteService {
    @Autowired
    private ContacteRepository contacteRepository;

    public ContacteServiceImpl (ContacteRepository contacteRepository){
        this.contacteRepository = contacteRepository;
    }
    public Contacte create (Contacte contacte){
        return contacteRepository .saveAndFlush(contacte);
    }
    @Override
    public List<Contacte> getAll() {return contacteRepository.findAll();}

    @Override
    public Contacte update(Contacte contacte) {
        return contacteRepository.saveAndFlush(contacte);
    }
    @Override
    public void delete(String id) {
        contacteRepository.deleteById(id);
    }
    @Override
    public Contacte getByCode(String code) {
        Optional <Contacte> firstByCode = contacteRepository.findFirstByCod(code);
        return firstByCode.orElse(new Contacte());
    }
}
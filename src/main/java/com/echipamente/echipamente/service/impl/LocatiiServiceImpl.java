package com.echipamente.echipamente.service.impl;

import com.echipamente.echipamente.entity.Locatii;
import com.echipamente.echipamente.repository.LocatiiRepository;
import com.echipamente.echipamente.service.LocatiiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("locationService")
public class LocatiiServiceImpl implements LocatiiService {
    @Autowired
    private LocatiiRepository locatiiRepository;

    @Override
    public Locatii create(Locatii locatii) {
      return locatiiRepository.saveAndFlush(locatii);
    }

    @Override
    public List<Locatii> getAll() {
        return locatiiRepository.findAll();
    }

    @Override
    public Locatii update(Locatii locatii) {
        return locatiiRepository.saveAndFlush(locatii);
    }
}

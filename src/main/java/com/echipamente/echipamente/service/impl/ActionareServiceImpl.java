package com.echipamente.echipamente.service.impl;

import com.echipamente.echipamente.entity.Actionare;
import com.echipamente.echipamente.repository.ActionareRepository;
import com.echipamente.echipamente.service.ActionareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ActionareServiceImpl implements ActionareService {
    @Autowired
    private ActionareRepository actionareRepository;

    @Override
    public Actionare create(Actionare actionare) {
        return actionareRepository.saveAndFlush(actionare);
    }

    @Override
    public List<Actionare> getAll() {
        return actionareRepository .findAll();
    }

    @Override
    public Actionare update(Actionare actionare) {
        return actionareRepository.saveAndFlush(actionare);
    }

    @Override
    public void delete(int id) {
        actionareRepository.deleteById(id);

    }
}

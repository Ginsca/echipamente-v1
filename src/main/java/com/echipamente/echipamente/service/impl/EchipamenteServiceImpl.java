package com.echipamente.echipamente.service.impl;

import com.echipamente.echipamente.entity.Contacte;
import com.echipamente.echipamente.entity.Echipamente;
import com.echipamente.echipamente.repository.ContacteRepository;
import com.echipamente.echipamente.repository.EchipamenteRepository;
import com.echipamente.echipamente.service.EchipamenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.Id;
import java.util.List;
import java.util.Optional;

@Service
public class EchipamenteServiceImpl implements EchipamenteService {

    @Autowired
    private EchipamenteRepository echipamenteRepository;

    @Autowired
    private ContacteRepository contacteRepository;

    public EchipamenteServiceImpl(EchipamenteRepository echipamenteRepository) {
        this.echipamenteRepository = echipamenteRepository;
    }

    public Echipamente create(Echipamente echipamente) {
        return echipamenteRepository.saveAndFlush(echipamente);
    }

    @Override
    public Echipamente getByCode(Integer code) {
        Optional<Echipamente> firstByCod = echipamenteRepository.findFirstByCod(code);
        return firstByCod.orElse(new Echipamente());
    }

    @Override
    public List<Echipamente> getAll() {
        return echipamenteRepository.findAll();
    }

    @Override
    public Echipamente update(Echipamente echipamente) {
        return echipamenteRepository.saveAndFlush(echipamente);
    }

    @Override
    public boolean delete(int id) {
        echipamenteRepository.deleteById(id);
        return true;
    }

    @Override
    public void addContact(int equipmentCode, String contactCode) {
        Echipamente echipament = echipamenteRepository.findFirstByCod(equipmentCode).orElse(new Echipamente());
        List<Contacte> contacte = echipament.getContacte();
        Contacte contact = contacteRepository.findFirstByCod(contactCode).orElse(new Contacte());
        contacte.add(contact);
         echipamenteRepository.saveAndFlush(echipament);
    }
}

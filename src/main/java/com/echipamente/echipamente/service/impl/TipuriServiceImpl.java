package com.echipamente.echipamente.service.impl;

import com.echipamente.echipamente.entity.Tipuri;
import com.echipamente.echipamente.repository.TipuriRepository;
import com.echipamente.echipamente.service.TipuriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("tipuriService")
public class TipuriServiceImpl implements TipuriService {

    @Autowired
    private TipuriRepository tipuriRepository;

    @Override
    public Tipuri create(Tipuri tipuri) {
        return tipuriRepository.saveAndFlush(tipuri);
    }

    @Override
    public List<Tipuri> getAll() {
        return tipuriRepository.findAll();
    }

    @Override
    public Tipuri update(Tipuri tipuri) {
        return tipuriRepository.saveAndFlush(tipuri);
    }

}





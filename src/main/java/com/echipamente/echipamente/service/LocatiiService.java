package com.echipamente.echipamente.service;

import com.echipamente.echipamente.entity.Locatii;

import java.util.List;

public interface LocatiiService {

    Locatii create(Locatii locatii);

    List<Locatii> getAll();

    Locatii update(Locatii locatii);
}

package com.echipamente.echipamente.service;


import java.util.List;

public interface Versiuni {
    Versiuni create(Versiuni versiuni);
    List<Versiuni> getAll();

}

package com.echipamente.echipamente.service;

import com.echipamente.echipamente.entity.Locatii;
import com.echipamente.echipamente.entity.Tipuri;

import java.util.List;

public interface TipuriService {
  Tipuri create(Tipuri tipuri);
    List<Tipuri> getAll();
    Tipuri update(Tipuri tipuri);


}

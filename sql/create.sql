CREATE DATABASE `echipamente` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE echipamente;

CREATE TABLE `versiuni` (
  `id` int NOT NULL AUTO_INCREMENT,
  `versiune` int NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `tipuri` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nume` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descriere` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `actionare` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nume` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descriere` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `locatii` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `echipamente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cod` int NOT NULL,
  `nume` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tip_echipament` int DEFAULT NULL,
  `producator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `actionare` int NOT NULL,
  `locatie` int NOT NULL,
  `observatie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tip_echipament` (`tip_echipament`),
  KEY `actionare` (`actionare`),
  KEY `locatie` (`locatie`),
  CONSTRAINT `echipamente_ibfk_1` FOREIGN KEY (`tip_echipament`) REFERENCES `tipuri` (`id`),
  CONSTRAINT `echipamente_ibfk_2` FOREIGN KEY (`actionare`) REFERENCES `actionare` (`id`),
  CONSTRAINT `echipamente_ibfk_3` FOREIGN KEY (`locatie`) REFERENCES `locatii` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `contacte` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cod` mediumtext NOT NULL,
  `nume` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `observatii` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `echipamente_has_contacte` (
  `echipamente_id` int NOT NULL,
  `contacte_id` int NOT NULL,
  PRIMARY KEY (`echipamente_id`,`contacte_id`),
  KEY `fk_echipamente_has_contacte_contacte1_idx` (`contacte_id`),
  KEY `fk_echipamente_has_contacte_echipamente1_idx` (`echipamente_id`),
  CONSTRAINT `fk_echipamente_has_contacte_contacte1` FOREIGN KEY (`contacte_id`) REFERENCES `contacte` (`id`),
  CONSTRAINT `fk_echipamente_has_contacte_echipamente1` FOREIGN KEY (`echipamente_id`) REFERENCES `echipamente` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO versiuni  (versiune, created) values (1, '2020-10-02');
